package ua.dp.eroy.th.fragments;


import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ua.dp.eroy.th.Const;
import ua.dp.eroy.th.R;
import ua.dp.eroy.th.Url;


public class FragmentAddEqToTP extends DialogFragment {


    private ImageButton bScan;
    private EditText etsn, etcomments, etbox;
    private Spinner spModel;
    private String model, sn, comments, box;
    private ProgressDialog mPD;
    private Button bAdd, bCancel;
    private String shtitle;
    private TextView tvtp;

    public FragmentAddEqToTP() {
        // Required empty public constructor
        // Required empty public constructor
        int style = DialogFragment.STYLE_NORMAL, theme = 0;
        theme = R.style.DialogCostom;
        setStyle(style, theme);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_add_eq_to_tp, container, false);
        getDialog().setTitle(R.string.addeqtotp);
        initPD();
        initObjects(v);


        readEqDB(spModel, Url.urlReadEq, mPD);


        bScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    Intent intent = new Intent("com.google.zxing.client.android.SCAN");
                    intent.putExtra("SCAN_FORMATS", "CODE_39,CODE_93,CODE_128,DATA_MATRIX,ITF,CODABAR,UPC_A,UPC_E,EAN_8,EAN_13,ITF,RSS_14,RSS_EXPANDED");

                    startActivityForResult(intent, 0);

                } catch (Exception e) {

                    Uri marketUri = Uri.parse("market://details?id=com.google.zxing.client.android");
                    Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);
                    startActivity(marketIntent);

                }

            }
        });


        bCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        bAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                insertTpDb(Url.urlNewTpdb);


            }
        });

        shtitle = String.valueOf(getArguments() != null ? getArguments().getString("shtitle") : 0);
        tvtp.setText(shtitle);
        return v;
    }


    private void initObjects(View v) {
        bScan = (ImageButton) v.findViewById(R.id.bScan);
        bAdd = (Button) v.findViewById(R.id.bAdd);
        bCancel = (Button) v.findViewById(R.id.bCancel);

        etsn = (EditText) v.findViewById(R.id.etsn);
        etcomments = (EditText) v.findViewById(R.id.etcomment);
        etbox = (EditText) v.findViewById(R.id.etbox);
        spModel = (Spinner) v.findViewById(R.id.spModel);
        spModel = (Spinner) v.findViewById(R.id.spModel);

        tvtp = (TextView) v.findViewById(R.id.tvtp);


    }


    private void initPD() {
        mPD = new ProgressDialog(getActivity());
        mPD.setMessage("Loading.....");
        mPD.setCancelable(false);

    }


    private void insertTpDb(String url) {
        mPD.show();

        getValue();


        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dismiss();
                        Toast.makeText(getActivity(),
                                "Data Inserted Successfully",
                                Toast.LENGTH_LONG).show();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getActivity(),
                        "failed to insert", Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("tp", shtitle);
                params.put("box", box);
                params.put("model", model);
                params.put("sn", sn);
                params.put("comments", comments);
                return params;
            }
        };
        mPD.dismiss();
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(postRequest);

    }

    private void getValue() {
        model = spModel.getSelectedItem().toString();
        sn = etsn.getText().toString();
        box = etbox.getText().toString();
        comments = etcomments.getText().toString();

    }


    private void readEqDB(final Spinner spModel1, String url, final ProgressDialog PD) {

        PD.show();
        final ArrayList<String> Item_List1 = new ArrayList<>();
        JsonObjectRequest jreq = new JsonObjectRequest(Request.Method.GET, url,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            int success = response.getInt("success");

                            if (success == 1) {
                                JSONArray ja = response.getJSONArray("orders");

                                for (int i = 0; i < ja.length(); i++) {

                                    JSONObject jobj = ja.getJSONObject(i);


                                    Item_List1.add(jobj.getString(Const.COLUMN_MODEL));
                                } // for loop ends


                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, Item_List1);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spModel1.setAdapter(adapter);

                                PD.dismiss();

                            } // if ends

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                PD.dismiss();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(jreq);


    }




    //Result scan and sn setText result
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == 0) {
            if (resultCode == getActivity().RESULT_OK) {
                etsn.setText(intent.getStringExtra("SCAN_RESULT"));
            } else if (resultCode == getActivity().RESULT_CANCELED) {
                Toast.makeText(getActivity(), "Error scan", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void onDismiss(DialogInterface dialog) {

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        super.onDismiss(dialog);


    }

    public void onCancel(DialogInterface dialog) {

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        super.onCancel(dialog);


    }

}
