package ua.dp.eroy.th.fragments;


import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ua.dp.eroy.th.R;
import ua.dp.eroy.th.adapter.TabAdapterEq;


public class FragmentEquipment extends Fragment {


    public FragmentEquipment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_equipmnet, container, false);
        initTabAdapter(v);

        return v;
    }

    private void initTabAdapter(View v) {
        FragmentManager fragmentManager = getFragmentManager();
        TabLayout tabLayout = (TabLayout) v.findViewById(R.id.tab_layout);

        tabLayout.addTab(tabLayout.newTab().setText(R.string.frEq));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.frTwo));

        final ViewPager viewPager = (ViewPager) v.findViewById(R.id.paper);

        TabAdapterEq tabAdapter = new TabAdapterEq(fragmentManager, 2);

        viewPager.setAdapter(tabAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

}
