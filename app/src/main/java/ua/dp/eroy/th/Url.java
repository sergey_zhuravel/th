package ua.dp.eroy.th;


public class Url {
    public static String urlReadCalc = "http://itad.dp.ua/android/read_calc.php";
    public static String urlNewCalc = "http://itad.dp.ua/android/newCalc.php";
    public static String urlDelItemCalc = "http://itad.dp.ua/android/delete_item_calc.php?id=";
    public static String urlNewFix = "http://itad.dp.ua/android/newFix.php";
    public static String urlReadFix = "http://itad.dp.ua/android/read_fix.php";
    public static String urlDelItemFix = "http://itad.dp.ua/android/delete_item_fix.php?id=";
    public static String urlReadRequest = "http://itad.dp.ua/android/read_request.php";
    public static String urlDelItemRequest = "http://itad.dp.ua/android/delete_item_request.php?id=";
    public static String urlNewRequest = "http://itad.dp.ua/android/newRequest.php";

    public static String urlReadEq = "http://itad.dp.ua/android/read_equipment.php";
    public static String urlNewTpdb = "http://itad.dp.ua/android/tpdb.php";

    public static String urlReadTp = "http://itad.dp.ua/android/read_tp.php";
    public static String urlNewTp = "http://itad.dp.ua/android/eqaddTP.php";
    public static String urlNewEq = "http://itad.dp.ua/android/equipment.php";

    public static String urlSearchTpDb = "http://itad.dp.ua/android/search_tpdb.php?tp=";
    public static String urlDelItemTpDb = "http://itad.dp.ua/android/delete_item_tpdb.php?id=";
    public static String urlUpdateTpDb = "http://itad.dp.ua/android/update_tpdb.php";
    public static String urlLoginDb = "http://itad.dp.ua/android/login.php";
    public static String urlAdminDb = "http://itad.dp.ua/android/admin.php";
    public static String urlReadUser = "http://itad.dp.ua/android/read_user.php";
    public static String urlNewUser = "http://itad.dp.ua/android/add_user.php";



}
