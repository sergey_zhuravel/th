package ua.dp.eroy.th.fragments.mainCalc;


import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import ua.dp.eroy.th.MainActivity;
import ua.dp.eroy.th.R;
import ua.dp.eroy.th.Url;
import ua.dp.eroy.th.utils.RequestHandler;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentHistoryCalcFull extends DialogFragment {

    private TextView tv;
    private ProgressDialog mPD;

    private String title, phone, address, descriptions, type, price, id;
    private Button bDel, bCall, bCancel;


    public FragmentHistoryCalcFull() {
        // Required empty public constructor
        int style = DialogFragment.STYLE_NORMAL, theme = 0;
        theme = R.style.DialogCostom;
        setStyle(style, theme);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_history_calc_full, container, false);

        bDel = (Button) v.findViewById(R.id.bDelete);
        bCall = (Button) v.findViewById(R.id.bCall);
        bCancel = (Button) v.findViewById(R.id.bCancel);
        tv = (TextView) v.findViewById(R.id.tv);

        initPd();

        tv.append(initialise());


        bCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        bDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmDeleteEmployee();

            }
        });
        bCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getValues();
                Uri uri = Uri.parse("tel:" + phone);
                Intent intent = new Intent(Intent.ACTION_DIAL, uri);
                startActivity(intent);


            }
        });

        return v;
    }

    private void initPd() {
        mPD = new ProgressDialog(getActivity());
        mPD.setMessage("please wait.....");
        mPD.setCancelable(false);
    }

    private String initialise() {
        getValues();

        String[] listArray = new String[]{"id: " + id, "Тип заявки: " + type, "Название: " + title, "Телефон: " + phone,
                "Адрес: " + address, "Описание: " + descriptions, "Цена: " + price};
        String textOut = "";

        for (int i = 0; i < listArray.length; i++) {
            textOut += listArray[i] + "\n";
        }

        return textOut;

    }

    private void getValues() {
        id = String.valueOf(getArguments() != null ? getArguments().getString("id") : 0);
        type = String.valueOf(getArguments() != null ? getArguments().getString("type") : 0);
        title = String.valueOf(getArguments() != null ? getArguments().getString("title") : 0);
        phone = String.valueOf(getArguments() != null ? getArguments().getString("phone") : 0);
        address = String.valueOf(getArguments() != null ? getArguments().getString("address") : 0);
        descriptions = String.valueOf(getArguments() != null ? getArguments().getString("descriptions") : 0);
        price = String.valueOf(getArguments() != null ? getArguments().getString("price") : 0);

    }

    private void confirmDeleteEmployee() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setMessage("Are you sure you want to delete this employee?");


        getValues();

        alertDialogBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        deleteEmployee(Url.urlDelItemCalc, id);
                        startActivity(new Intent(getActivity(), MainActivity.class));
                    }
                });

        alertDialogBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void deleteEmployee(final String url, final String id) {
        class DeleteEmployee extends AsyncTask<Void, Void, String> {
            ProgressDialog loading;


            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(getActivity(), "Updating...", "Wait...", false, false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(getActivity(), s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequestParam(url, id);
                return s;
            }
        }

        DeleteEmployee de = new DeleteEmployee();
        de.execute();
    }


}
