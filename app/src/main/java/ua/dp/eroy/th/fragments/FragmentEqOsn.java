package ua.dp.eroy.th.fragments;


import android.app.DialogFragment;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ua.dp.eroy.th.Const;
import ua.dp.eroy.th.R;
import ua.dp.eroy.th.Url;


public class FragmentEqOsn extends Fragment implements SearchView.OnQueryTextListener {


    private ProgressDialog mPD;
    private EditText etShtitle, etTitle, etAddress, etComments,
            etmodel, etports;
    private Spinner spType;
    private String shtitle, title, address, comments, model, ports, type;
    private ListView lv;
    private ArrayList<HashMap<String, String>> Item_List;
    private ListAdapter adapter;
    private DialogFragment df;

    private SearchView sw;

    private String[] item = {
            "Добавить оборудование на ТП", "Добавить Техническую площадку", "Новое оборудование"
    };

    public FragmentEqOsn() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_eq_osn, container, false);
        initAdapter(v);

        return v;
    }


    private void initAdapter(View v) {

        ListView lv = (ListView) v.findViewById(R.id.frListForm);

        lv.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, item));

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        dialogNewEqToTp();
                        break;
                    case 1:
                        dialogNewTp();

                        break;
                    case 2:
                        dialogNewEq();

                        break;

                }
            }
        });
    }

    private void dialogNewEqToTp() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(),R.style.AlertDialogFullScreen);
        initEtEqtoTo(dialogBuilder);

        dialogBuilder.setTitle(getString(R.string.fr_dialogNewEqToTp));
        mPD = ProgressDialog.show(getActivity(), "",
                "Loading...", true);
        new Thread(new Runnable() {
            public void run() {
                readTpDB(Url.urlReadTp);
            }
        }).start();


        lv.setOnItemClickListener(new ListitemClickListener());
        registerForContextMenu(lv);


        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }


    private void dialogNewEq() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());


        initEtEq(dialogBuilder);
        dialogBuilder.setTitle(getString(R.string.fr_newEq));

        dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                model = etmodel.getText().toString();
                ports = etports.getText().toString();
                type = spType.getSelectedItem().toString();
                comments = etComments.getText().toString();

                mPD = ProgressDialog.show(getActivity(), "",
                        "Loading...", true);
                new Thread(new Runnable() {
                    public void run() {
                        insertEqDB(Url.urlNewEq);
                    }
                }).start();

            }


        });

        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();


    }


    private void dialogNewTp() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());


        initEtTp(dialogBuilder);

        dialogBuilder.setTitle(getString(R.string.fr_newTP));
        dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                shtitle = etShtitle.getText().toString();
                title = etTitle.getText().toString();
                address = etAddress.getText().toString();
                comments = etComments.getText().toString();

                mPD = ProgressDialog.show(getActivity(), "",
                        "Loading...", true);
                new Thread(new Runnable() {
                    public void run() {
                        insertDB(Url.urlNewTp);
                    }
                }).start();

            }


        });

        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();

    }

    private void initEtEqtoTo(AlertDialog.Builder dialogBuilder) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_newertotp, null);
        dialogBuilder.setView(dialogView);

        lv = (ListView) dialogView.findViewById(R.id.lv);
        sw = (SearchView) dialogView.findViewById(R.id.search_view);

        lv.setTextFilterEnabled(true);

//    bget = (Button) dialogView.findViewById(R.id.bget);
        setupSearchView();


    }

    private void setupSearchView() {
        sw.setIconifiedByDefault(false);
        sw.setOnQueryTextListener(this);
        sw.setSubmitButtonEnabled(false);
        sw.setQueryHint("Search Here");
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        if (TextUtils.isEmpty(query)) {
            lv.clearTextFilter();
        } else {
            lv.setFilterText(query.toString());
        }
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    private void initEtEq(AlertDialog.Builder dialogBuilder) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_neweq, null);
        dialogBuilder.setView(dialogView);

        etmodel = (EditText) dialogView.findViewById(R.id.etmodel);
        etports = (EditText) dialogView.findViewById(R.id.etports);
        etComments = (EditText) dialogView.findViewById(R.id.etcomment);
        spType = (Spinner) dialogView.findViewById(R.id.spType);
    }

    private void initEtTp(AlertDialog.Builder dialogBuilder) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_newtp, null);
        dialogBuilder.setView(dialogView);

        etShtitle = (EditText) dialogView.findViewById(R.id.etshtitle);
        etTitle = (EditText) dialogView.findViewById(R.id.ettitle);
        etAddress = (EditText) dialogView.findViewById(R.id.etaddress);
        etComments = (EditText) dialogView.findViewById(R.id.etcomment);
    }


    private void insertDB(String url) {
        mPD.show();

        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mPD.dismiss();
                        Toast.makeText(getActivity(),
                                "Data Inserted Successfully",
                                Toast.LENGTH_LONG).show();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getActivity(),
                        "failed to insert", Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("shtitle", shtitle);
                params.put("title", title);
                params.put("address", address);
                params.put("comments", comments);
                return params;
            }
        };
        mPD.dismiss();
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(postRequest);
    }

    private void insertEqDB(String url) {
        mPD.show();

        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mPD.dismiss();
                        Toast.makeText(getActivity(),
                                "Data Inserted Successfully",
                                Toast.LENGTH_LONG).show();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getActivity(),
                        "failed to insert", Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("model", model);
                params.put("type", type);
                params.put("ports", ports);
                params.put("comments", comments);
                return params;
            }
        };
        mPD.dismiss();
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(postRequest);
    }

    private void readTpDB(String url) {
        mPD.show();

        Item_List = new ArrayList<>();
        JsonObjectRequest jreq = new JsonObjectRequest(Request.Method.GET, url,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            int success = response.getInt("success");

                            if (success == 1) {
                                JSONArray ja = response.getJSONArray("orders");

                                for (int i = 0; i < ja.length(); i++) {

                                    JSONObject jobj = ja.getJSONObject(i);
                                    HashMap<String, String> item = new HashMap<String, String>();
                                    item.put(Const.COLUMN_SHTITLE, jobj.getString(Const.COLUMN_SHTITLE));
                                    item.put(Const.COLUMN_TITLE, jobj.getString(Const.COLUMN_TITLE));
                                    item.put(Const.COLUMN_ADDRESS, jobj.getString(Const.COLUMN_ADDRESS));
                                    item.put(Const.COLUMN_COMMENTS, jobj.getString(Const.COLUMN_COMMENTS));

                                    Item_List.add(item);


                                } // for loop ends


                                String[] from = {Const.COLUMN_SHTITLE, Const.COLUMN_TITLE, Const.COLUMN_ADDRESS, Const.COLUMN_COMMENTS};
                                int[] to = {R.id.text1, R.id.text2, R.id.text3, R.id.text4};

                                adapter = new SimpleAdapter(
                                        getActivity(), Item_List,
                                        R.layout.item_eqtotp, from, to);

                                lv.setAdapter(adapter);

                                mPD.dismiss();

                            } // if ends

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                mPD.dismiss();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(jreq);


    }


    private class ListitemClickListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            df = new FragmentAddEqToTP();


            Bundle bundle = new Bundle();
            HashMap<String, String> map = (HashMap) parent.getItemAtPosition(position);

            bundle.putString("shtitle", map.get(Const.COLUMN_SHTITLE));
            bundle.putString("title", map.get(Const.COLUMN_TITLE));
            bundle.putString("address", map.get(Const.COLUMN_ADDRESS));
            bundle.putString("comments", map.get(Const.COLUMN_COMMENTS));


            df.setArguments(bundle);

            df.show(getActivity().getFragmentManager(), "df");


        }
    }
}
