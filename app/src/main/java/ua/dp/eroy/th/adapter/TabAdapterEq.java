package ua.dp.eroy.th.adapter;


import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentStatePagerAdapter;

import ua.dp.eroy.th.fragments.FragmentEqOsn;
import ua.dp.eroy.th.fragments.FragmentHistoryTp;

public class TabAdapterEq extends FragmentStatePagerAdapter {
    private int mNumberOfTabs;

    public TabAdapterEq(FragmentManager fm, int mNumberOfTabs) {
        super(fm);
        this.mNumberOfTabs = mNumberOfTabs;
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:
                return new FragmentEqOsn();

            case 1:
                return new FragmentHistoryTp();



            default:
                return null;

        }
    }

    @Override
    public int getCount() {
        return mNumberOfTabs;
    }
}
