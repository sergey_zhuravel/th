package ua.dp.eroy.th.fragments.mainForm;


import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import ua.dp.eroy.th.Const;
import ua.dp.eroy.th.MainActivity;
import ua.dp.eroy.th.R;
import ua.dp.eroy.th.Url;
import ua.dp.eroy.th.utils.MailSenderClass;


public class FragmentFix extends DialogFragment implements View.OnClickListener {


    private String email1, email2;
    private ProgressDialog mPD;
    private ArrayList<HashMap<String, String>> text1;
    private Spinner fix;

    private String login, phone, address, descriptions, type;


    private EditText input_login, input_phone, input_address, input_desc;
    private Button bCancel, bSend, bSave;

    public FragmentFix() {
        // Required empty public constructor
        int style = DialogFragment.STYLE_NORMAL, theme = 0;
        theme = R.style.DialogCostom;
        setStyle(style, theme);
    }


    //Sortirovka HashMap
    public static <K extends Comparable, V extends Comparable> Map<K, V> sortByKeys(Map<K, V> map) {
        List<K> keys = new LinkedList<K>(map.keySet());
        Collections.sort(keys);

        //LinkedHashMap will keep the keys in the order they are inserted
        //which is currently sorted on natural ordering
        Map<K, V> sortedMap = new LinkedHashMap<K, V>();
        for (K key : keys) {
            sortedMap.put(key, map.get(key));
        }

        return sortedMap;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_dialog_fix, null);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        getDialog().setTitle(R.string.fr_requstFix);

        input_login = (EditText) v.findViewById(R.id.etlogin);
        input_phone = (EditText) v.findViewById(R.id.etPN);
        input_address = (EditText) v.findViewById(R.id.etAddress);
        input_desc = (EditText) v.findViewById(R.id.etDescriptions);


        bCancel = (Button) v.findViewById(R.id.bCancel);
        bCancel.setOnClickListener(this);
        bSend = (Button) v.findViewById(R.id.bSend);
        bSend.setOnClickListener(this);
        bSave = (Button) v.findViewById(R.id.bSave);
        bSave.setOnClickListener(this);


        fix = (Spinner) v.findViewById(R.id.fix_array);

        return v;
    }


    private void insert(String url) {
        mPD.show();

        type = fix.getSelectedItem().toString();
        phone = input_phone.getText().toString();
        login = input_login.getText().toString();
        address = input_address.getText().toString();
        descriptions = input_desc.getText().toString();


        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mPD.dismiss();
                        Toast.makeText(getActivity(),
                                "Data Inserted Successfully",
                                Toast.LENGTH_LONG).show();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getActivity(),
                        "failed to insert", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("type", type);
                params.put("login", login);
                params.put("phone", phone);
                params.put("address", address);
                params.put("descriptions", descriptions);
                return params;
            }
        };
        mPD.dismiss();
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(postRequest);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bCancel:
                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                dismiss();
                break;
            case R.id.bSave:
                mPD = ProgressDialog.show(getActivity(), "",
                        "Loading...", true);
                new Thread(new Runnable() {
                    public void run() {
                        insert(Url.urlNewFix);
                    }
                }).start();

                startActivity(new Intent(getActivity(), MainActivity.class));
                break;
            case R.id.bSend:
                mPD = ProgressDialog.show(getActivity(), "",
                        "Loading...", true);
                new Thread(new Runnable() {
                    public void run() {
                        insert(Url.urlNewFix);
                    }
                }).start();


                sender_mail_async async_sending = new sender_mail_async();
                async_sending.execute();
                break;

        }
    }

    public void onDismiss(DialogInterface dialog) {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        super.onDismiss(dialog);


    }

    public void onCancel(DialogInterface dialog) {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        super.onCancel(dialog);


    }


    private class sender_mail_async extends AsyncTask<Object, String, Boolean> {
        ProgressDialog WaitingDialog;


        @Override
        protected void onPreExecute() {

            //find elements

            login = ((EditText) getView().findViewById(R.id.etlogin)).getText().toString();
            phone = ((EditText) getView().findViewById(R.id.etPN)).getText().toString();
            address = ((EditText) getView().findViewById(R.id.etAddress)).getText().toString();
            descriptions = ((EditText) getView().findViewById(R.id.etDescriptions)).getText().toString();

            type = fix.getSelectedItem().toString();

            //email send where
            email1 = ((Spinner) getView().findViewById(R.id.spEmail)).getSelectedItem().toString();
            if (email1.equals("Other")) {
                email2 = ((EditText) getView().findViewById(R.id.etSpEmail)).getText().toString();
                email1 = email2;
            }

            text1 = new ArrayList<HashMap<String, String>>();
            //add elements to Hashmap
            HashMap<String, String> text2 = new HashMap<>();
            text2.put("\n01.Тип проблемы", type);
            text2.put("\n02.Номер договора/логин", login);
            text2.put("\n03.Телефон", phone);
            text2.put("\n04.Адрес", address);
            text2.put("\n05.Описание", descriptions);

            HashMap<String, String> sorted = (HashMap<String, String>) sortByKeys(text2);

            text1.add(sorted);

            WaitingDialog = ProgressDialog.show(getActivity(), "Отправка заявки", "Запрос обрабатывается.\n Пожалуйста подождите...", true);

        }


        @Override
        protected void onPostExecute(Boolean result) {

            WaitingDialog.dismiss();
            Toast.makeText(getActivity(), "Запрос отправлен!!", Toast.LENGTH_LONG).show();
            startActivity(new Intent(getActivity(), MainActivity.class));
            dismiss();
        }

        @Override
        protected Boolean doInBackground(Object... params) {

            try {

                MailSenderClass sender = new MailSenderClass(Const.SEND_FROM, Const.SEND_PASSWORD);

                sender.sendMail("Заявка на ремонт", text1, Const.SEND_FROM, email1);
            } catch (Exception e) {
                Toast.makeText(getActivity(), "?????? ???????? ?????????!", Toast.LENGTH_SHORT).show();
            }

            return false;
        }
    }


}
