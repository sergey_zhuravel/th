package ua.dp.eroy.th.fragments.admin;


import android.app.Fragment;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ua.dp.eroy.th.Const;
import ua.dp.eroy.th.MainActivity;
import ua.dp.eroy.th.R;
import ua.dp.eroy.th.Url;


public class FragmentAdmin extends Fragment {

    // Идентификатор уведомления
    private static final int NOTIFY_ID = 101;
    private ArrayList<String> list = new ArrayList<>();


    private Button bAddLoginDB, bReadLoginDB, bPush;
    private ListAdapter adapter;
    private ListView lv;
    private ProgressDialog mPD;
    private String username, password;
    private EditText etlogin, etpass;
    private String a;

    private TextView tv;

    public FragmentAdmin() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_admin, container, false);


        bAddLoginDB = (Button) v.findViewById(R.id.bAddLoginDB);
        bPush = (Button) v.findViewById(R.id.bPush);
        bAddLoginDB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogNewUser();
            }
        });

        bReadLoginDB = (Button) v.findViewById(R.id.bReadLoginDB);
        bReadLoginDB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogReadUsers();
            }
        });

        tv = (TextView) v.findViewById(R.id.tv);

        bPush.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPD = ProgressDialog.show(getActivity(), "",
                        "Validating user...", true);
                new Thread(new Runnable() {
                    public void run() {
                        readIdLogin(Url.urlReadUser);
                    }
                }).start();


            }
        });

        return v;
    }

    private void dialogNewUser() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());

        initEtNewUsers(dialogBuilder);

        dialogBuilder.setTitle(getString(R.string.fr_userInBd));

        dialogBuilder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mPD = ProgressDialog.show(getActivity(), "",
                        "Loading...", true);
                new Thread(new Runnable() {
                    public void run() {
                        newUsersDb(Url.urlNewUser);
                    }
                }).start();
            }
        });

        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    private void initEtNewUsers(AlertDialog.Builder dialogBuilder) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.fragment_admin_add_login_db, null);
        dialogBuilder.setView(dialogView);

        etlogin = (EditText) dialogView.findViewById(R.id.etlogin);
        etpass = (EditText) dialogView.findViewById(R.id.etpass);


    }

    public void newUsersDb(String url) {
        mPD.show();
        username = etlogin.getText().toString();
        password = etpass.getText().toString();

        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        etlogin.setText("");
                        etpass.setText("");
                        Toast.makeText(getActivity(),
                                "Data Inserted Successfully",
                                Toast.LENGTH_LONG).show();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getActivity(),
                        "failed to insert", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", username);
                params.put("password", password);

                return params;
            }
        };
        mPD.dismiss();
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(postRequest);

    }

    private void dialogReadUsers() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());


        initEtUsers(dialogBuilder);

        dialogBuilder.setTitle(getString(R.string.fr_userInBd));
        mPD = ProgressDialog.show(getActivity(), "",
                "Validating user...", true);
        new Thread(new Runnable() {
            public void run() {
                readFromLoginDB(Url.urlReadUser);
            }
        }).start();


        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();


    }

    private void initEtUsers(AlertDialog.Builder dialogBuilder) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.fragment_admin_read_login_db, null);
        dialogBuilder.setView(dialogView);

        lv = (ListView) dialogView.findViewById(R.id.listView);


    }


    private void readFromLoginDB(String url) {
        mPD.show();

        final ArrayList<HashMap<String, String>> Item_List = new ArrayList<HashMap<String, String>>();


        JsonObjectRequest jreq = new JsonObjectRequest(Request.Method.GET, url,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            int success = response.getInt("success");

                            if (success == 1) {
                                JSONArray ja = response.getJSONArray(Const.COLUMN_ARRAY);

                                for (int i = 0; i < ja.length(); i++) {

                                    JSONObject jobj = ja.getJSONObject(i);
                                    HashMap<String, String> item = new HashMap<String, String>();
                                    item.put(Const.COLUMN_ID, jobj.getString(Const.COLUMN_ID));
                                    item.put(Const.COLUMN_NAME,
                                            jobj.getString(Const.COLUMN_NAME));

                                    Item_List.add(item);

                                    list.add(jobj.getString(Const.COLUMN_ID));


                                } // for loop ends

                                String[] from = {Const.COLUMN_ID, Const.COLUMN_NAME};
                                int[] to = {R.id.tvId, R.id.tvUser};

                                adapter = new SimpleAdapter(
                                        getActivity(), Item_List,
                                        R.layout.item_list_read, from, to);

                                lv.setAdapter(adapter);

                                mPD.dismiss();

                            } // if ends

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                mPD.dismiss();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(jreq);

    }

    private void readIdLogin(String url) {

        mPD.show();
        JsonObjectRequest jreq = new JsonObjectRequest(Request.Method.GET, url,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            int success = response.getInt("success");

                            if (success == 1) {
                                JSONArray ja = response.getJSONArray(Const.COLUMN_ARRAY);

                                for (int i = 0; i < ja.length(); i++) {

                                    JSONObject jobj = ja.getJSONObject(i);

                                    list.add(jobj.getString(Const.COLUMN_ID));

                                } // for loop ends


                                a = String.valueOf(list.size());


                                tv.setText(a);

                                mPD.dismiss();
                            } // if ends


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                mPD.dismiss();
            }
        });


        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(jreq);

    }

    private void sms(Context context, String str) {

        readFromLoginDB(Url.urlReadUser);


        Intent notificationIntent = new Intent(context, MainActivity.class);
        notificationIntent.putExtra("str", str);


        PendingIntent contentIntent = PendingIntent.getActivity(context,
                0, notificationIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);


        Notification.Builder builder = new Notification.Builder(getActivity());

        builder.setContentIntent(contentIntent)
                .setSmallIcon(android.R.drawable.alert_dark_frame)
                .setContentTitle("Напоминание")
                .setContentText("Пора покормить кота"); // Текст уведомления

        Notification notification = builder.build();

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getActivity());
        notificationManager.notify(NOTIFY_ID, notification);


    }


}
