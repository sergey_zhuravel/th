package ua.dp.eroy.th.fragments.mainCalc;


import android.app.DialogFragment;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

import ua.dp.eroy.th.R;
import ua.dp.eroy.th.Url;
import ua.dp.eroy.th.item.Item;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentCalcS extends DialogFragment {

    private String title, phone, address, descriptions, type;
    private TextView total;
    private Spinner spinnerType;
    private EditText eTitle, eAddress, ePhone, eDescriptions;
    private ProgressDialog PD;
    private String mPrice;

    private Button bCalc, bSave, bClear,bCancel;
    private TextView tv;
    private EditText e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16;
    private double d, d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19;
    private CheckBox c1, c2, c3;

    public FragmentCalcS() {
        // Required empty public constructor
        int style = DialogFragment.STYLE_NORMAL, theme = 0;
        theme = R.style.AlertDialogFullScreen;
        setStyle(style, theme);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_calc_s, container, false);
        getDialog().setTitle(R.string.ft_calcStud);


        initObjects(v);

        bCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearValuesEt();
                dismiss();
            }
        });
        bClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearValuesEt();

            }
        });


        bCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcElements();

                tv.setText(mPrice);


            }
        });


        bSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcElements();
               dialogSave();
            }
        });
        return v;

    }


    private void dialogSave() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());


        initEtSave(dialogBuilder);
        dialogBuilder.setTitle("Save Calc");

        dialogBuilder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                PD = ProgressDialog.show(getActivity(), "",
                        "Loading...", true);
                new Thread(new Runnable() {
                    public void run() {
                        insert(Url.urlNewCalc);
                    }
                }).start();
            }


        });

        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                clearValuesEt();
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    private void initEtSave(AlertDialog.Builder dialogBuilder) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.fragment_fragment_calc_save, null);
        dialogBuilder.setView(dialogView);

        eTitle = (EditText) dialogView.findViewById(R.id.eTitle);
        eAddress = (EditText) dialogView.findViewById(R.id.eAddress);
        eDescriptions = (EditText) dialogView.findViewById(R.id.etDescriptions);
        spinnerType = (Spinner) dialogView.findViewById(R.id.spinnerType);
        ePhone = (EditText) dialogView.findViewById(R.id.ePhone);
        total = (TextView) dialogView.findViewById(R.id.total);


        total.setText(mPrice);
    }

    public void insert(String url) {
        PD.show();
        type = spinnerType.getSelectedItem().toString();
        phone = ePhone.getText().toString();
        title = eTitle.getText().toString();
        address = eAddress.getText().toString();
        descriptions = eDescriptions.getText().toString();

        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dismiss();
                        Toast.makeText(getActivity(),
                                "Data Inserted Successfully",
                                Toast.LENGTH_LONG).show();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getActivity(),
                        "failed to insert", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("title", title);
                params.put("phone", phone);
                params.put("address", address);
                params.put("descriptions", descriptions);
                params.put("type", type);
                params.put("price", mPrice);
                return params;
            }
        };
        PD.dismiss();
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(postRequest);

    }


    private void calcElements() {
        parseDouble();

        if (c1.isChecked()) {
            d17 = 1;
        } else {
            d17 = 0;
        }
        if (c2.isChecked()) {
            d18 = 1;
        } else {
            d18 = 0;
        }
        if (c3.isChecked()) {
            d19 = 1;
        } else {
            d19 = 0;
        }

        d = Item.cable(d1) + Item.cable1(d2) + Item.korobka(d3) + Item.gofra(d4) + Item.electric(d5) + Item.podves(d6) +
                Item.korob(d7) + Item.pribivashki(d8) + Item.stripsy(d9) + Item.router(d10) + Item.locoM2(d11) + Item.poe(d12) +
                Item.filtrset(d13) + Item.switch8p(d14) + Item.sfp(d15) + Item.rj45(d16) + Item.car(d17) + Item.work2ch(d18) + Item.work3ch(d19);

        mPrice = Double.toString(d);
    }

    private void parseDouble() {
        d1 = Double.parseDouble(equal((e1).getText().toString()));
        d2 = Double.parseDouble(equal((e2).getText().toString()));
        d3 = Double.parseDouble(equal((e3).getText().toString()));
        d4 = Double.parseDouble(equal((e4).getText().toString()));
        d5 = Double.parseDouble(equal((e5).getText().toString()));
        d6 = Double.parseDouble(equal((e6).getText().toString()));
        d7 = Double.parseDouble(equal((e7).getText().toString()));
        d8 = Double.parseDouble(equal((e8).getText().toString()));
        d9 = Double.parseDouble(equal((e9).getText().toString()));
        d10 = Double.parseDouble(equal((e10).getText().toString()));
        d11 = Double.parseDouble(equal((e11).getText().toString()));
        d12 = Double.parseDouble(equal((e12).getText().toString()));
        d13 = Double.parseDouble(equal((e13).getText().toString()));
        d14 = Double.parseDouble(equal((e14).getText().toString()));
        d15 = Double.parseDouble(equal((e15).getText().toString()));
        d16 = Double.parseDouble(equal((e16).getText().toString()));
    }

    private void clearValuesEt() {
        e1.getText().clear();
        e2.getText().clear();
        e3.getText().clear();
        e4.getText().clear();
        e5.getText().clear();
        e6.getText().clear();
        e7.getText().clear();
        e8.getText().clear();
        e9.getText().clear();
        e10.getText().clear();
        e11.getText().clear();
        e12.getText().clear();
        e13.getText().clear();
        e14.getText().clear();
        e15.getText().clear();
        e16.getText().clear();
        c1.setChecked(false);
        c2.setChecked(false);
        c3.setChecked(false);

        tv.setText("0.0");
    }

    private void initObjects(View v) {
        e1 = (EditText) v.findViewById(R.id.vp);
        e2 = (EditText) v.findViewById(R.id.vp1);
        e3 = (EditText) v.findViewById(R.id.korobka);
        e4 = (EditText) v.findViewById(R.id.gofra);
        e5 = (EditText) v.findViewById(R.id.elect);
        e6 = (EditText) v.findViewById(R.id.podves);
        e7 = (EditText) v.findViewById(R.id.korob);
        e8 = (EditText) v.findViewById(R.id.pribivashki);
        e9 = (EditText) v.findViewById(R.id.stripsy);
        e10 = (EditText) v.findViewById(R.id.router);
        e11 = (EditText) v.findViewById(R.id.locoM2);
        e12 = (EditText) v.findViewById(R.id.poe);
        e13 = (EditText) v.findViewById(R.id.filtrst);
        e14 = (EditText) v.findViewById(R.id.switch8p);
        e15 = (EditText) v.findViewById(R.id.sfp);
        e16 = (EditText) v.findViewById(R.id.rj45);

        c1 = (CheckBox) v.findViewById(R.id.car);
        c2 = (CheckBox) v.findViewById(R.id.work2ch);
        c3 = (CheckBox) v.findViewById(R.id.work3ch);
        tv = (TextView) v.findViewById(R.id.tv);
        bClear = (Button) v.findViewById(R.id.bClear);
        bCalc = (Button) v.findViewById(R.id.bCalc);
        bSave = (Button) v.findViewById(R.id.bSave);
        bCancel = (Button) v.findViewById(R.id.bCancel);
    }

    public String equal(String s1) {
        if (s1.equals("")) {
            s1 = String.valueOf(0);
        }
        return s1;
    }
}
