package ua.dp.eroy.th.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {


    private static final String DATABASE_NAME = "userstore.db"; // название бд
    private static final int SCHEMA = 7; // версия базы данных
    public static final String TABLE = "users"; // название таблицы в бд
    public static final String TABLE1 = "stud"; // название таблицы в бд
    public static final String TABLE2 = "fix"; // название таблицы в бд
    public static final String TABLE3 = "sigin"; // название таблицы в бд
    // названия столбцов
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_ADDRESS = "address";
    public static final String COLUMN_TOTAL = "total";
    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_PHONE = "phone";

    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_LNAME = "lname";
    public static final String COLUMN_MNAME = "mname";
    public static final String COLUMN_PASSPORT = "passport";
    public static final String COLUMN_INN = "inn";
    public static final String COLUMN_STREET = "street";
    public static final String COLUMN_BUILD = "build";
    public static final String COLUMN_FLAT = "flat";
    public static final String COLUMN_HOSTEL = "hostel";
    public static final String COLUMN_NHOSTEL = "nhostel";
    public static final String COLUMN_RATE = "rate";


    public static final String COLUMN_PASSWORD = "password";

    public static final String COLUMN_LOGIN = "login";
    public static final String COLUMN_DESK = "desk";

    public DBHelper(Context context) {

        super(context, DATABASE_NAME, null, SCHEMA);
    }

    public DBHelper(Context context, String name,SQLiteDatabase.CursorFactory factory, int version)
    {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE users (" + COLUMN_ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT," + COLUMN_TYPE
                + " TEXT, " + COLUMN_TITLE + " TEXT, " + COLUMN_ADDRESS + " TEXT, " + COLUMN_PHONE + " TEXT, "   + COLUMN_TOTAL + " TEXT);");

        db.execSQL("CREATE TABLE stud (" + COLUMN_ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT," + COLUMN_NAME
                + " TEXT, " + COLUMN_LNAME + " TEXT, " + COLUMN_MNAME + " TEXT, " + COLUMN_PASSPORT + " TEXT, " + COLUMN_INN + " TEXT, " + COLUMN_STREET + " TEXT, " + COLUMN_BUILD + " TEXT, " + COLUMN_FLAT + " TEXT, "
                + COLUMN_HOSTEL + " TEXT, " + COLUMN_NHOSTEL + " TEXT, " + COLUMN_PHONE + " TEXT, "  + COLUMN_RATE + " TEXT);");

        db.execSQL("CREATE TABLE fix (" + COLUMN_ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT," + COLUMN_TYPE
                + " TEXT, " + COLUMN_LOGIN + " TEXT, " + COLUMN_PHONE + " TEXT, " + COLUMN_ADDRESS + " TEXT, "   + COLUMN_DESK + " TEXT);");

        /*db.execSQL("CREATE TABLE sigin (" + COLUMN_ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT," + COLUMN_LOGIN
                + " TEXT, "  + COLUMN_PASSWORD + " TEXT);");*/





        // добавление начальных данных
//        db.execSQL("INSERT INTO " + TABLE3 + " (" + COLUMN_LOGIN + ", " + COLUMN_PASSWORD + ") VALUES ('serj', 'admin');");
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion,  int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE1);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE2);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE3);
        onCreate(db);
    }

}