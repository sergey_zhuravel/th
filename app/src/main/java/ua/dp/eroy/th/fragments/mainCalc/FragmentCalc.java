package ua.dp.eroy.th.fragments.mainCalc;


import android.app.DialogFragment;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import ua.dp.eroy.th.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentCalc extends Fragment {

    private static DialogFragment dlg1,dlg2,dlg3;

    private String[] item = {
            "Радио", "Выделенщики", "История"

    };

    public FragmentCalc() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_calc, container, false);

        dlg1 = new FragmentCalcR();
        dlg2 = new FragmentCalcS();
        dlg3 = new FragmentCalcHistory();

        ListView lv = (ListView) v.findViewById(R.id.frListForm);
        lv.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, item));

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        dlg1.show(getActivity().getFragmentManager(), "dlg1");
                        break;
                    case 1:
                        dlg2.show(getActivity().getFragmentManager(), "dlg2");
                        break;
                    case 2:
                        dlg3.show(getActivity().getFragmentManager(), "dlg3");
                        break;


                }
            }
        });

        return v;
    }

}
