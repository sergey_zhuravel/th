package ua.dp.eroy.th.fragments.mainForm;


import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import ua.dp.eroy.th.Const;
import ua.dp.eroy.th.MainActivity;
import ua.dp.eroy.th.R;
import ua.dp.eroy.th.Url;
import ua.dp.eroy.th.utils.DBHelper;
import ua.dp.eroy.th.utils.MailSenderClass;


public class FragmentNewRequest extends DialogFragment implements View.OnClickListener {

    private DBHelper dbHelper;

    private String fio, phone, passport, inn, street, build, room, hostel, nhostel, rate;
    private ProgressDialog mPD;
    private String email1, email2;
    private ArrayList<HashMap<String, String>> text;
    private EditText input_fio, input_pass, input_inn, input_phone, input_street, input_build, input_room, etSpEmail;
    private Spinner sphostel, spnhostel, sprate, email;
    private Button bCancel, bSave, bSend;

    public FragmentNewRequest() {
        // Required empty public constructor
        int style = DialogFragment.STYLE_NORMAL, theme = 0;
        theme = R.style.DialogCostom;
        setStyle(style, theme);
    }


    //Sortirovka HashMap
    public static <K extends Comparable, V extends Comparable> Map<K, V> sortByKeys(Map<K, V> map) {
        List<K> keys = new LinkedList<K>(map.keySet());
        Collections.sort(keys);

        //LinkedHashMap will keep the keys in the order they are inserted
        //which is currently sorted on natural ordering
        Map<K, V> sortedMap = new LinkedHashMap<K, V>();
        for (K key : keys) {
            sortedMap.put(key, map.get(key));
        }

        return sortedMap;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_dialog_req_stud, null);
        getDialog().setTitle(R.string.fr_newRequest);


        dbHelper = new DBHelper(getActivity());

        initObjects(v);


        bCancel.setOnClickListener(this);
        bSave.setOnClickListener(this);
        bSend.setOnClickListener(this);

        return v;
    }

    private void initObjects(View v) {
        input_fio = (EditText) v.findViewById(R.id.etfio);
        input_phone = (EditText) v.findViewById(R.id.etPN);
        input_pass = (EditText) v.findViewById(R.id.etPasport);
        input_inn = (EditText) v.findViewById(R.id.etINN);
        input_street = (EditText) v.findViewById(R.id.etAStreet);
        input_build = (EditText) v.findViewById(R.id.etABuild);
        input_room = (EditText) v.findViewById(R.id.etARoom);
        etSpEmail = (EditText) v.findViewById(R.id.etSpEmail);

        sphostel = (Spinner) v.findViewById(R.id.spHostel);
        spnhostel = (Spinner) v.findViewById(R.id.spNHostel);
        sprate = (Spinner) v.findViewById(R.id.spRate);
        email = (Spinner) v.findViewById(R.id.spEmail);

        bCancel = (Button) v.findViewById(R.id.bCancel);
        bSave = (Button) v.findViewById(R.id.bSave);
        bSend = (Button) v.findViewById(R.id.bSend);
    }

    public void insert(String url) {
        mPD.show();
        getTextElements();

        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mPD.dismiss();
                        Toast.makeText(getActivity(),
                                "Data Inserted Successfully",
                                Toast.LENGTH_LONG).show();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getActivity(),
                        "failed to insert", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("fio", fio);
                params.put("phone", phone);
                params.put("passport", passport);
                params.put("inn", inn);
                params.put("street", street);
                params.put("build", build);
                params.put("room", room);
                params.put("hostel", hostel);
                params.put("nhostel", nhostel);
                params.put("rate", rate);

                return params;
            }
        };
        mPD.dismiss();
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(postRequest);

    }

    private void getTextElements() {
        fio = input_fio.getText().toString();
        phone = input_phone.getText().toString();
        passport = input_pass.getText().toString();
        inn = input_inn.getText().toString();
        street = input_street.getText().toString();
        build = input_build.getText().toString();
        room = input_room.getText().toString();
        hostel = sphostel.getSelectedItem().toString();
        nhostel = spnhostel.getSelectedItem().toString();
        rate = sprate.getSelectedItem().toString();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.bCancel:
                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                dismiss();
                break;

            case R.id.bSave:
                mPD = ProgressDialog.show(getActivity(), "",
                        "Loading...", true);
                new Thread(new Runnable() {
                    public void run() {
                        insert(Url.urlNewRequest);
                    }
                }).start();


                startActivity(new Intent(getActivity(), MainActivity.class));
                break;

            case R.id.bSend:
                mPD = ProgressDialog.show(getActivity(), "",
                        "Loading...", true);
                new Thread(new Runnable() {
                    public void run() {
                        insert(Url.urlNewRequest);
                    }
                }).start();


                sender_mail_async async_sending = new sender_mail_async();
                async_sending.execute();


                break;

        }
    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);


    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);


    }

    private class sender_mail_async extends AsyncTask<Object, String, Boolean> {
        ProgressDialog WaitingDialog;


        @Override
        protected void onPreExecute() {

            getTextElements();

            //email send where
            email1 = ((Spinner) getView().findViewById(R.id.spEmail)).getSelectedItem().toString();
            if (email1.equals("Other")) {
                email2 = ((EditText) getView().findViewById(R.id.etSpEmail)).getText().toString();
                email1 = email2;
            }

            text = new ArrayList<HashMap<String, String>>();
            //add elements to Hashmap
            HashMap<String, String> text2 = new HashMap<>();
            text2.put("\n01.ФИО", fio);
            text2.put("\n02.Телефон", phone);
            text2.put("\n03.Пасспорт", passport);
            text2.put("\n04.ИНН", inn);
            text2.put("\n05.Улица", street);
            text2.put("\n06.Дом", build);
            text2.put("\n07.Комната", room);
            text2.put("\n08.Общежитие", hostel);
            text2.put("\n09.№", nhostel);
            text2.put("\n10.Тариф", rate);

            HashMap<String, String> sorted = (HashMap<String, String>) sortByKeys(text2);

            text.add(sorted);

            WaitingDialog = ProgressDialog.show(getActivity(), "Отправка заявки", "Запрос обрабатывается.\n Пожалуйста подождите...", true);

        }


        @Override
        protected void onPostExecute(Boolean result) {

            WaitingDialog.dismiss();
            Toast.makeText(getActivity(), "Запрос отправлен!!", Toast.LENGTH_LONG).show();
            startActivity(new Intent(getActivity(), MainActivity.class));
            dismiss();
        }

        @Override
        protected Boolean doInBackground(Object... params) {

            try {

                MailSenderClass sender = new MailSenderClass(Const.SEND_FROM, Const.SEND_PASSWORD);

                sender.sendMail("Заявка на подключения", text, Const.SEND_FROM, email1);
            } catch (Exception e) {
                Toast.makeText(getActivity(), "?????? ???????? ?????????!", Toast.LENGTH_SHORT).show();
            }

            return false;
        }
    }

}
