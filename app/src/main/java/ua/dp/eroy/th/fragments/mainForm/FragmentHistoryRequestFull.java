package ua.dp.eroy.th.fragments.mainForm;


import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

import ua.dp.eroy.th.Const;
import ua.dp.eroy.th.MainActivity;
import ua.dp.eroy.th.R;
import ua.dp.eroy.th.Url;
import ua.dp.eroy.th.utils.MailSenderClass;
import ua.dp.eroy.th.utils.RequestHandler;

public class FragmentHistoryRequestFull extends DialogFragment {

    private TextView tv;
    private ProgressDialog mPD;
    private String id, fio, phone, passport, inn, street, build, room, hostel, nhostel, rate;
    private Button bDel, bSend, bCall,bCancel;
    private String email1, email2;

    public FragmentHistoryRequestFull() {
        // Required empty public constructor
        int style = DialogFragment.STYLE_NORMAL, theme = 0;
        theme = R.style.DialogCostom;
        setStyle(style, theme);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_history_request_full, container, false);
        initObjects(v);

        initPd();


        tv.append(initialise());

        bCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getValues();
                Uri uri = Uri.parse("tel:" + phone);
                Intent intent = new Intent(Intent.ACTION_DIAL, uri);
                startActivity(intent);
            }
        });

        bDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmDeleteEmployee();
            }
        });
        bSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
        dialogSend();



            }
        });
        bCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return v;
    }

    private void initObjects(View v) {
        bDel = (Button) v.findViewById(R.id.bDelete);
        bSend = (Button) v.findViewById(R.id.bSend);
        bCall = (Button) v.findViewById(R.id.bCall);
        bCancel = (Button) v.findViewById(R.id.bCancel);
        tv = (TextView) v.findViewById(R.id.tv);
    }

    private void initPd() {
        mPD = new ProgressDialog(getActivity());
        mPD.setMessage("please wait.....");
        mPD.setCancelable(false);
    }

    private String initialise() {
        getValues();

        String[] listArray = new String[]{"id: " + id, "ФИО: " + fio, "Телефон: " + phone, "Пасспорт: " + passport,
                "ИНН: " + inn, "Улица: " + street, "Дом: " + build, "Комнатна: " + room, "Общежитие: " + hostel,
                "№: " + nhostel, "Тариф: " + rate};
        String textOut = "";

        for (int i = 0; i < listArray.length; i++) {
            textOut += listArray[i] + "\n";
        }

        return textOut;

    }

    private void getValues() {
        id = String.valueOf(getArguments() != null ? getArguments().getString("id") : 0);
        fio = String.valueOf(getArguments() != null ? getArguments().getString("fio") : 0);
        phone = String.valueOf(getArguments() != null ? getArguments().getString("phone") : 0);
        passport = String.valueOf(getArguments() != null ? getArguments().getString("passport") : 0);
        inn = String.valueOf(getArguments() != null ? getArguments().getString("inn") : 0);
        street = String.valueOf(getArguments() != null ? getArguments().getString("street") : 0);
        build = String.valueOf(getArguments() != null ? getArguments().getString("build") : 0);
        room = String.valueOf(getArguments() != null ? getArguments().getString("room") : 0);
        hostel = String.valueOf(getArguments() != null ? getArguments().getString("hostel") : 0);
        nhostel = String.valueOf(getArguments() != null ? getArguments().getString("nhostel") : 0);
        rate = String.valueOf(getArguments() != null ? getArguments().getString("rate") : 0);
    }

    private void confirmDeleteEmployee() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setMessage("Are you sure you want to delete this employee?");


        getValues();

        alertDialogBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        deleteEmployee(Url.urlDelItemRequest,id);
                        startActivity(new Intent(getActivity(), MainActivity.class));
                    }
                });

        alertDialogBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void deleteEmployee(final String url, final String id) {
        class DeleteEmployee extends AsyncTask<Void, Void, String> {
            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(getActivity(), "Updating...", "Wait...", false, false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(getActivity(), s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequestParam(url, id);
                return s;
            }
        }

        DeleteEmployee de = new DeleteEmployee();
        de.execute();
    }


    private void dialogSend() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());


        initEtSend(dialogBuilder);
        dialogBuilder.setTitle(getString(R.string.fr_repeatSend));

        dialogBuilder.setPositiveButton("Send", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                sender_mail_async async_sending = new sender_mail_async();
                async_sending.execute();

            }


        });

        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();

    }

    private void initEtSend(AlertDialog.Builder dialogBuilder) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.fragment_history_fix_send, null);
        dialogBuilder.setView(dialogView);


        email1 = ((Spinner) dialogView.findViewById(R.id.spEmail)).getSelectedItem().toString();
        email2 = ((EditText) dialogView.findViewById(R.id.etSpEmail)).getText().toString();


    }

    private class sender_mail_async extends AsyncTask<Object, String, Boolean> {
        ProgressDialog WaitingDialog;
        private ArrayList<HashMap<String, String>> text;

        @Override
        protected void onPreExecute() {

            getValues();


            //email send where
            if (email1.equals("Other")) {
                email1 = email2;
            }

            text = new ArrayList<HashMap<String, String>>();
            //add elements to Hashmap
            HashMap<String, String> text2 = new HashMap<>();
            text2.put("\n01.ФИО", fio);
            text2.put("\n02.Телефон", phone);
            text2.put("\n03.Пасспорт", passport);
            text2.put("\n04.ИНН", inn);
            text2.put("\n05.Улица", street);
            text2.put("\n06.Дом", build);
            text2.put("\n07.Комната", room);
            text2.put("\n08.Общежитие", hostel);
            text2.put("\n09.№", nhostel);
            text2.put("\n10.Тариф", rate);

            HashMap<String, String> sorted = (HashMap<String, String>) FragmentNewRequest.sortByKeys(text2);

            text.add(sorted);

            WaitingDialog = ProgressDialog.show(getActivity(), "Отправка заявки", "Запрос обрабатывается.\n Пожалуйста подождите...", true);

        }


        @Override
        protected void onPostExecute(Boolean result) {

            WaitingDialog.dismiss();
            Toast.makeText(getActivity(), "Запрос отправлен!!", Toast.LENGTH_LONG).show();
            startActivity(new Intent(getActivity(), MainActivity.class));
            dismiss();
        }

        @Override
        protected Boolean doInBackground(Object... params) {

            try {

                MailSenderClass sender = new MailSenderClass(Const.SEND_FROM, Const.SEND_PASSWORD);

                sender.sendMail("Заявка на подключения", text, Const.SEND_FROM, email1);
            } catch (Exception e) {
                Toast.makeText(getActivity(), "?????? ???????? ?????????!", Toast.LENGTH_SHORT).show();
            }

            return false;
        }
    }

}
