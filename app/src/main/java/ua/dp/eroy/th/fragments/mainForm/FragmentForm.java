package ua.dp.eroy.th.fragments.mainForm;


import android.app.DialogFragment;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import ua.dp.eroy.th.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentForm extends Fragment {

    private DialogFragment mDlg1, mDlg2, mDlg3, mDlg4;


    private String[] item = {
            "Новая заявка", "Новая проблема", "История заявок", "История проблем"
    };

    public FragmentForm() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_form, container, false);
        initAdapter(v);


        return v;
    }

    private void initAdapter(View v) {


        ListView lv = (ListView) v.findViewById(R.id.frListForm);

        lv.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, item));

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        mDlg1 = new FragmentNewRequest();
                        mDlg1.show(getActivity().getFragmentManager(), "mDlg1");
                        break;
                    case 1:
                        mDlg2 = new FragmentFix();
                        mDlg2.show(getActivity().getFragmentManager(), "mDlg2");
                        break;
                    case 2:
                        mDlg3 = new FragmentHistoryRequest();
                        mDlg3.show(getActivity().getFragmentManager(), "mDlg3");
                        break;
                    case 3:
                        mDlg4 = new FragmentHistoryFix();
                        mDlg4.show(getActivity().getFragmentManager(), "mDlg4");
                        break;
                }
            }
        });
    }


}
