package ua.dp.eroy.th.fragments.mainCalc;


import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SimpleAdapter;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import ua.dp.eroy.th.Const;
import ua.dp.eroy.th.R;
import ua.dp.eroy.th.Url;


public class FragmentCalcHistory extends DialogFragment implements AdapterView.OnItemClickListener, SearchView.OnQueryTextListener {


    private ListView mList;
    private SearchView sw;
    private ArrayList<HashMap<String, String>> mItem_List;
    private ProgressDialog mPD;
    private ListAdapter mAdapter;
    private DialogFragment df;


    public FragmentCalcHistory() {
        int style = DialogFragment.STYLE_NORMAL, theme = 0;
        theme = R.style.AlertDialogFullScreen;
        setStyle(style, theme);
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_history_calc, container, false);

        mList = (ListView) v.findViewById(R.id.lv);
        mList.setTextFilterEnabled(true);

        mItem_List = new ArrayList<HashMap<String, String>>();
        sw = (SearchView) v.findViewById(R.id.search_view);

        initPd();

        setupSearchView();

        mList.setOnItemClickListener(this);
        ReadDataFromDB(Url.urlReadCalc);
        registerForContextMenu(mList);


        return v;
    }

    private void setupSearchView() {
        sw.setFocusableInTouchMode(false);
        sw.setFocusable(false);

        sw.setIconifiedByDefault(false);
        sw.setOnQueryTextListener(this);
        sw.setSubmitButtonEnabled(false);
        sw.setQueryHint("Search Here");
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        if (TextUtils.isEmpty(query)) {
            mList.clearTextFilter();
        } else {
            mList.setFilterText(query.toString());
        }
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    private void initPd() {
        mPD = new ProgressDialog(getActivity());
        mPD.setMessage("Loading.....");
        mPD.setCancelable(false);
    }


    private void ReadDataFromDB(String url) {
        mPD.show();
        JsonObjectRequest jreq = new JsonObjectRequest(Request.Method.GET, url,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            int success = response.getInt("success");

                            if (success == 1) {
                                JSONArray ja = response.getJSONArray("orders");

                                for (int i = 0; i < ja.length(); i++) {

                                    JSONObject jobj = ja.getJSONObject(i);
                                    HashMap<String, String> item = new HashMap<String, String>();
                                    item.put(Const.COLUMN_ID, jobj.getString(Const.COLUMN_ID));
                                    item.put(Const.COLUMN_TITLE, jobj.getString(Const.COLUMN_TITLE));
                                    item.put(Const.COLUMN_PHONE, jobj.getString(Const.COLUMN_PHONE));
                                    item.put(Const.COLUMN_ADDRESS, jobj.getString(Const.COLUMN_ADDRESS));
                                    item.put(Const.COLUMN_DESCRIPTIONS, jobj.getString(Const.COLUMN_DESCRIPTIONS));
                                    item.put(Const.COLUMN_TYPE, jobj.getString(Const.COLUMN_TYPE));
                                    item.put(Const.COLUMN_PRICE, jobj.getString(Const.COLUMN_PRICE));


                                    mItem_List.add(item);

                                } // for loop ends

                                String[] from = {Const.COLUMN_ID, Const.COLUMN_TYPE, Const.COLUMN_TITLE, Const.COLUMN_PHONE,
                                        Const.COLUMN_ADDRESS, Const.COLUMN_DESCRIPTIONS, Const.COLUMN_PRICE};
                                int[] to = {R.id.text1, R.id.text2, R.id.text3, R.id.text4, R.id.text5, R.id.text6, R.id.text7};

                                mAdapter = new SimpleAdapter(
                                        getActivity(), mItem_List,
                                        R.layout.item_calc, from, to);

                                mList.setAdapter(mAdapter);

                                mPD.dismiss();

                            } // if ends

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                mPD.dismiss();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(jreq);


    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        df = new FragmentHistoryCalcFull();

        Bundle bundle = new Bundle();
        HashMap<String, String> map = (HashMap) parent.getItemAtPosition(position);

        bundle.putString("id", map.get(Const.COLUMN_ID));
        bundle.putString("type", map.get(Const.COLUMN_TYPE));
        bundle.putString("title", map.get(Const.COLUMN_TITLE));
        bundle.putString("phone", map.get(Const.COLUMN_PHONE));
        bundle.putString("address", map.get(Const.COLUMN_ADDRESS));
        bundle.putString("descriptions", map.get(Const.COLUMN_DESCRIPTIONS));
        bundle.putString("price", map.get(Const.COLUMN_PRICE));


        df.setArguments(bundle);

        df.show(getActivity().getFragmentManager(), "df");
    }
}


