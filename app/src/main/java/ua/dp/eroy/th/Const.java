package ua.dp.eroy.th;


public class Const {
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_PHONE = "phone";
    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_ADDRESS = "address";
    public static final String COLUMN_DESCRIPTIONS = "descriptions";
    public static final String COLUMN_PRICE = "price";
    public static final String COLUMN_FIO = "fio";
    public static final String COLUMN_PASSPORT = "passport";
    public static final String COLUMN_INN = "inn";
    public static final String COLUMN_STREET = "street";
    public static final String COLUMN_BUILD = "build";
    public static final String COLUMN_ROOM = "room";
    public static final String COLUMN_HOSTEL = "hostel";
    public static final String COLUMN_NHOSTEL = "nhostel";
    public static final String COLUMN_RATE = "rate";
    public static final String COLUMN_MODEL = "model";
    public static final String COLUMN_SHTITLE = "shtitle";
    public static final String COLUMN_COMMENTS = "comments";
    public static final String COLUMN_TP = "tp";
    public static final String COLUMN_SN = "sn";
    public static final String COLUMN_BOX = "box";
    public static final String COLUMN_ARRAY = "orders";
    public static final String COLUMN_LOGIN = "login";
    public static final String COLUMN_NAME = "username";

    public static final String SEND_FROM = "trifle.test@gmail.com";
    public static final String SEND_PASSWORD = "trifle_best";


}
