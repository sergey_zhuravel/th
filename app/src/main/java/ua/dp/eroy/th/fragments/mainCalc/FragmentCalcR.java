package ua.dp.eroy.th.fragments.mainCalc;


import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

import ua.dp.eroy.th.R;
import ua.dp.eroy.th.Url;
import ua.dp.eroy.th.item.Item;


public class FragmentCalcR extends DialogFragment {

    private Spinner spinnerType;
    private EditText eTitle, eAddress, ePhone, eDescriptions;
    private ProgressDialog PD;
    private String mPrice;
    private Button bCalc, bSave, bClear, bCancel;
    private TextView tv;
    private EditText e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13;
    private double d, d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12, d13, d14, d15;
    private CheckBox c1, c2;
    private String title, phone, address, descriptions, type;
    private TextView total;


    public FragmentCalcR() {
        // Required empty public constructor
        int style = DialogFragment.STYLE_NORMAL, theme = 0;
        theme = R.style.AlertDialogFullScreen;
        setStyle(style, theme);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_calc_r, container, false);
        getDialog().setTitle(R.string.fr_calcRadio);


        initObjects(v);


        bCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearValuesEt();
                dismiss();
            }
        });
        bClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearValuesEt();

            }
        });


        bCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                calcElements();

                tv.setText(mPrice);


            }
        });


        bSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcElements();
                dialogSave();

            }
        });


        return v;
    }

    private void dialogSave() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());


        initEtSave(dialogBuilder);
        dialogBuilder.setTitle("Save Calc");

        dialogBuilder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                PD = ProgressDialog.show(getActivity(), "",
                        "Loading...", true);
                new Thread(new Runnable() {
                    public void run() {
                        insert(Url.urlNewCalc);
                    }
                }).start();
            }


        });

        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    private void initEtSave(AlertDialog.Builder dialogBuilder) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.fragment_fragment_calc_save, null);
        dialogBuilder.setView(dialogView);

        eTitle = (EditText) dialogView.findViewById(R.id.eTitle);
        eAddress = (EditText) dialogView.findViewById(R.id.eAddress);
        eDescriptions = (EditText) dialogView.findViewById(R.id.etDescriptions);
        spinnerType = (Spinner) dialogView.findViewById(R.id.spinnerType);
        ePhone = (EditText) dialogView.findViewById(R.id.ePhone);
        total = (TextView) dialogView.findViewById(R.id.total);


        total.setText(mPrice);
    }

    public void insert(String url) {
        PD.show();
        type = spinnerType.getSelectedItem().toString();
        phone = ePhone.getText().toString();
        title = eTitle.getText().toString();
        address = eAddress.getText().toString();
        descriptions = eDescriptions.getText().toString();

        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dismiss();
                        Toast.makeText(getActivity(),
                                "Data Inserted Successfully",
                                Toast.LENGTH_LONG).show();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getActivity(),
                        "failed to insert", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("title", title);
                params.put("phone", phone);
                params.put("address", address);
                params.put("descriptions", descriptions);
                params.put("type", type);
                params.put("price", mPrice);
                return params;
            }
        };
        PD.dismiss();
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(postRequest);

    }


    private void calcElements() {
        parseDouble();

        if (c1.isChecked()) {
            d14 = 1;
        } else {
            d14 = 0;
        }
        if (c2.isChecked()) {
            d15 = 1;
        } else {
            d15 = 0;
        }

        d = Item.cable(d1) + Item.podves(d2) + Item.beamM5(d3) + Item.korob(d4) + Item.pribivashki(d5) + Item.stripsy(d6) +
                Item.router(d7) + Item.locoM2(d8) + Item.M2(d9) + Item.kranstain(d10) + Item.locoM5(d11) + Item.M5(d12) + Item.rj45(d13) +
                Item.car(d14) + Item.work(d15);

        mPrice = Double.toString(d);
    }

    private void parseDouble() {
        d1 = Double.parseDouble(equal((e1).getText().toString()));
        d2 = Double.parseDouble(equal((e2).getText().toString()));
        d3 = Double.parseDouble(equal((e3).getText().toString()));
        d4 = Double.parseDouble(equal((e4).getText().toString()));
        d5 = Double.parseDouble(equal((e5).getText().toString()));
        d6 = Double.parseDouble(equal((e6).getText().toString()));
        d7 = Double.parseDouble(equal((e7).getText().toString()));
        d8 = Double.parseDouble(equal((e8).getText().toString()));
        d9 = Double.parseDouble(equal((e9).getText().toString()));
        d10 = Double.parseDouble(equal((e10).getText().toString()));
        d11 = Double.parseDouble(equal((e11).getText().toString()));
        d12 = Double.parseDouble(equal((e12).getText().toString()));
        d13 = Double.parseDouble(equal((e13).getText().toString()));
    }

    private void clearValuesEt() {
        e1.getText().clear();
        e2.getText().clear();
        e3.getText().clear();
        e4.getText().clear();
        e5.getText().clear();
        e6.getText().clear();
        e7.getText().clear();
        e8.getText().clear();
        e9.getText().clear();
        e10.getText().clear();
        e11.getText().clear();
        e12.getText().clear();
        e13.getText().clear();
        c1.setChecked(false);
        c2.setChecked(false);

        tv.setText("0.0");
    }

    private void initObjects(View v) {
        bClear = (Button) v.findViewById(R.id.bClear);
        bCalc = (Button) v.findViewById(R.id.bCalc);
        bSave = (Button) v.findViewById(R.id.bSave);
        bCancel = (Button) v.findViewById(R.id.bCancel);

        e1 = (EditText) v.findViewById(R.id.vp);
        e2 = (EditText) v.findViewById(R.id.podves);
        e3 = (EditText) v.findViewById(R.id.beamM5);
        e4 = (EditText) v.findViewById(R.id.korob);
        e5 = (EditText) v.findViewById(R.id.pribivashki);
        e6 = (EditText) v.findViewById(R.id.stripsy);
        e7 = (EditText) v.findViewById(R.id.router);
        e8 = (EditText) v.findViewById(R.id.locoM2);
        e9 = (EditText) v.findViewById(R.id.M2);
        e10 = (EditText) v.findViewById(R.id.kranstain);
        e11 = (EditText) v.findViewById(R.id.locoM5);
        e12 = (EditText) v.findViewById(R.id.M5);
        e13 = (EditText) v.findViewById(R.id.rj45);
        c1 = (CheckBox) v.findViewById(R.id.car);
        c2 = (CheckBox) v.findViewById(R.id.work);
        tv = (TextView) v.findViewById(R.id.tv);
    }


    public String equal(String s1) {
        if (s1.equals("")) {
            s1 = String.valueOf(0);
        }
        return s1;
    }
}