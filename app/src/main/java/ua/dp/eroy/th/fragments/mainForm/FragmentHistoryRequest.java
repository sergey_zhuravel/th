package ua.dp.eroy.th.fragments.mainForm;


import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SimpleAdapter;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import ua.dp.eroy.th.Const;
import ua.dp.eroy.th.R;
import ua.dp.eroy.th.Url;

public class FragmentHistoryRequest extends DialogFragment implements SearchView.OnQueryTextListener {

    private ArrayList<HashMap<String, String>> Item_List;
    private ProgressDialog mPD;
    private ListAdapter adapter;
    private ListView mLv;
    private DialogFragment df;
    private SearchView sw;


    public FragmentHistoryRequest() {
        // Required empty public constructor
        int style = DialogFragment.STYLE_NORMAL, theme = 0;
        theme = R.style.AlertDialogFullScreen;
        setStyle(style, theme);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_history_request, container, false);


        mLv = (ListView) v.findViewById(R.id.lvHReq);
        mLv.setTextFilterEnabled(true);

        Item_List = new ArrayList<HashMap<String, String>>();
        sw = (SearchView) v.findViewById(R.id.search_view);
        setupSearchView();
        initPd();


        mLv.setOnItemClickListener(new ListitemClickListener());
        ReadDataFromDB(Url.urlReadRequest);
        registerForContextMenu(mLv);

        return v;
    }

    private void setupSearchView() {
        sw.setFocusableInTouchMode(false);
        sw.setFocusable(false);

        sw.setIconifiedByDefault(false);
        sw.setOnQueryTextListener(this);
        sw.setSubmitButtonEnabled(false);
        sw.setQueryHint("Search Here");
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        if (TextUtils.isEmpty(query)) {
            mLv.clearTextFilter();
        } else {
            mLv.setFilterText(query.toString());
        }
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    private void initPd() {
        mPD = new ProgressDialog(getActivity());
        mPD.setMessage("Loading.....");
        mPD.setCancelable(false);
    }

    private void ReadDataFromDB(String url) {
        mPD.show();
        JsonObjectRequest jreq = new JsonObjectRequest(Request.Method.GET, url,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            int success = response.getInt("success");

                            if (success == 1) {
                                JSONArray ja = response.getJSONArray("orders");

                                for (int i = 0; i < ja.length(); i++) {

                                    JSONObject jobj = ja.getJSONObject(i);
                                    HashMap<String, String> item = new HashMap<String, String>();
                                    item.put(Const.COLUMN_ID, jobj.getString(Const.COLUMN_ID));
                                    item.put(Const.COLUMN_FIO, jobj.getString(Const.COLUMN_FIO));
                                    item.put(Const.COLUMN_PHONE, jobj.getString(Const.COLUMN_PHONE));
                                    item.put(Const.COLUMN_PASSPORT, jobj.getString(Const.COLUMN_PASSPORT));
                                    item.put(Const.COLUMN_INN, jobj.getString(Const.COLUMN_INN));
                                    item.put(Const.COLUMN_STREET, jobj.getString(Const.COLUMN_STREET));
                                    item.put(Const.COLUMN_BUILD, jobj.getString(Const.COLUMN_BUILD));
                                    item.put(Const.COLUMN_ROOM, jobj.getString(Const.COLUMN_ROOM));
                                    item.put(Const.COLUMN_HOSTEL, jobj.getString(Const.COLUMN_HOSTEL));
                                    item.put(Const.COLUMN_NHOSTEL, jobj.getString(Const.COLUMN_NHOSTEL));
                                    item.put(Const.COLUMN_RATE, jobj.getString(Const.COLUMN_RATE));


                                    Item_List.add(item);

                                } // for loop ends

                                String[] from = {Const.COLUMN_ID, Const.COLUMN_FIO, Const.COLUMN_PHONE, Const.COLUMN_PASSPORT,
                                        Const.COLUMN_INN, Const.COLUMN_STREET, Const.COLUMN_BUILD, Const.COLUMN_ROOM, Const.COLUMN_HOSTEL,
                                        Const.COLUMN_NHOSTEL, Const.COLUMN_RATE};
                                int[] to = {R.id.text1, R.id.text2, R.id.text3, R.id.text4, R.id.text5, R.id.text6, R.id.text7, R.id.text8,
                                        R.id.text9, R.id.text10, R.id.text11};

                                adapter = new SimpleAdapter(
                                        getActivity(), Item_List,
                                        R.layout.item_request, from, to);

                                mLv.setAdapter(adapter);

                                mPD.dismiss();

                            } // if ends

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                mPD.dismiss();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(jreq);


    }


    class ListitemClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            df = new FragmentHistoryRequestFull();
            Bundle bundle = new Bundle();
            HashMap<String, String> map = (HashMap) parent.getItemAtPosition(position);

            bundle.putString("id", map.get(Const.COLUMN_ID));
            bundle.putString("fio", map.get(Const.COLUMN_FIO));
            bundle.putString("phone", map.get(Const.COLUMN_PHONE));
            bundle.putString("passport", map.get(Const.COLUMN_PASSPORT));
            bundle.putString("inn", map.get(Const.COLUMN_INN));
            bundle.putString("street", map.get(Const.COLUMN_STREET));
            bundle.putString("build", map.get(Const.COLUMN_BUILD));
            bundle.putString("room", map.get(Const.COLUMN_ROOM));
            bundle.putString("hostel", map.get(Const.COLUMN_HOSTEL));
            bundle.putString("nhostel", map.get(Const.COLUMN_NHOSTEL));
            bundle.putString("rate", map.get(Const.COLUMN_RATE));


            df.setArguments(bundle);

            df.show(getActivity().getFragmentManager(), "df");


        }


    }


}
