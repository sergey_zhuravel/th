package ua.dp.eroy.th.fragments;


import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import ua.dp.eroy.th.Const;
import ua.dp.eroy.th.R;
import ua.dp.eroy.th.Url;
import ua.dp.eroy.th.utils.RequestHandler;


public class FragmentHistoryTp extends Fragment implements AdapterView.OnItemClickListener, SearchView.OnQueryTextListener {


    private TextView tvtp, tvmodel;
    private ArrayList<String> Item_List;
    private ArrayList<HashMap<String, String>> Item_List1;
    private ProgressDialog PD;
    private ArrayAdapter<String> adapter1;
    private ListAdapter adapter;
    private ListView lv;
    private Spinner sptp;
    private ImageButton bScan;
    private EditText etsn, etcomments, etbox;
    private Spinner spModel;
    private JSONArray ja;

    private SearchView sw;
    private String mId;


    public FragmentHistoryTp() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_history_tp, container, false);


        Item_List = new ArrayList<>();
        Item_List1 = new ArrayList<HashMap<String, String>>();

        lv = (ListView) v.findViewById(R.id.lv);

        sw = (SearchView) v.findViewById(R.id.search_view);

        initSpinner(v);


        setupSearchView();
        initPD();
        ReadDataFromDB(Url.urlReadTp);





        lv.setOnItemClickListener(this);
        return v;
    }




    private void initSpinner(View v) {
        sptp = (Spinner) v.findViewById(R.id.sptp);
        sptp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (sptp.getSelectedItem().toString().equals("")) {

                } else {
                    Item_List1.clear();
                    searchDBTP(Url.urlSearchTpDb, sptp.getSelectedItem().toString());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setupSearchView() {
        sw.setIconifiedByDefault(false);
        sw.setOnQueryTextListener(this);
        sw.setSubmitButtonEnabled(false);
        sw.setQueryHint("Search Here");
    }

    private void initPD() {
        PD = new ProgressDialog(getActivity());
        PD.setMessage("Loading.....");
        PD.setCancelable(false);

    }


    private void searchDBTP(String url, String value) {
        String url1;
        PD.show();


        url1 = url + value;

        StringRequest stringRequest = new StringRequest(url1, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                PD.dismiss();
                showJSON(response);
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity(), error.getMessage().toString(), Toast.LENGTH_LONG).show();
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    private void showJSON(String response) {

        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray result = jsonObject.getJSONArray(Const.COLUMN_ARRAY);
            for (int i = 0; i < result.length(); i++) {
                JSONObject jobj = result.getJSONObject(i);
                HashMap<String, String> item = new HashMap<String, String>();
                item.put(Const.COLUMN_ID, jobj.getString(Const.COLUMN_ID));
                item.put(Const.COLUMN_TP, jobj.getString(Const.COLUMN_TP));
                item.put(Const.COLUMN_BOX, jobj.getString(Const.COLUMN_BOX));
                item.put(Const.COLUMN_MODEL, jobj.getString(Const.COLUMN_MODEL));
                item.put(Const.COLUMN_SN, jobj.getString(Const.COLUMN_SN));
                item.put(Const.COLUMN_COMMENTS, jobj.getString(Const.COLUMN_COMMENTS));


                Item_List1.add(item);
            }
            String[] from = {Const.COLUMN_BOX, Const.COLUMN_MODEL, Const.COLUMN_SN, Const.COLUMN_COMMENTS};
            int[] to = {R.id.tvbox, R.id.tvmodel, R.id.tvsn, R.id.tvcomment};

            adapter = new SimpleAdapter(
                    getActivity(), Item_List1,
                    R.layout.item_history_tpdb, from, to);


            lv.setAdapter(adapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    private void ReadDataFromDB(String url) {
        PD.show();

        JsonObjectRequest jreq = new JsonObjectRequest(Request.Method.GET, url,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            int success = response.getInt("success");

                            if (success == 1) {
                                ja = response.getJSONArray("orders");

                                for (int i = 0; i < ja.length(); i++) {

                                    JSONObject jobj = ja.getJSONObject(i);


                                    Item_List.add(jobj.getString(Const.COLUMN_SHTITLE));


                                } // for loop ends

                                adapter1 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, Item_List);
                                adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                sptp.setAdapter(adapter1);

                                PD.dismiss();

                            } // if ends

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                PD.dismiss();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(jreq);


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        HashMap<String, String> map = (HashMap) parent.getItemAtPosition(position);

        mId = map.get(Const.COLUMN_ID);

        dialogUpdate(Url.urlUpdateTpDb);

        tvtp.setText(map.get(Const.COLUMN_TP));
        etbox.setText(map.get(Const.COLUMN_BOX));
        tvmodel.setText(map.get(Const.COLUMN_MODEL));
        etsn.setText(map.get(Const.COLUMN_SN));
        etcomments.setText(map.get(Const.COLUMN_COMMENTS));

        readEqDB(spModel, Url.urlReadEq, PD);


    }


    private void dialogUpdate(final String url) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());


        initUpdate(dialogBuilder);

        dialogBuilder.setTitle("Update TP");
        dialogBuilder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                updateEmployee(url);

            }


        });

        dialogBuilder.setNegativeButton("Delete", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                confirmDeleteEmployee();

            }
        });

        dialogBuilder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();

    }

    private void initUpdate(AlertDialog.Builder dialogBuilder) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_updatetp, null);
        dialogBuilder.setView(dialogView);

        bScan = (ImageButton) dialogView.findViewById(R.id.bScan);

        etsn = (EditText) dialogView.findViewById(R.id.etsn);
        etcomments = (EditText) dialogView.findViewById(R.id.etcomment);
        etbox = (EditText) dialogView.findViewById(R.id.etbox);
        spModel = (Spinner) dialogView.findViewById(R.id.spModel);

        tvtp = (TextView) dialogView.findViewById(R.id.tvtp);
        tvmodel = (TextView) dialogView.findViewById(R.id.tvmodel);


        bScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    Intent intent = new Intent("com.google.zxing.client.android.SCAN");
                    intent.putExtra("SCAN_FORMATS", "CODE_39,CODE_93,CODE_128,DATA_MATRIX,ITF,CODABAR,UPC_A,UPC_E,EAN_8,EAN_13,ITF,RSS_14,RSS_EXPANDED");

                    startActivityForResult(intent, 0);

                } catch (Exception e) {

                    Uri marketUri = Uri.parse("market://details?id=com.google.zxing.client.android");
                    Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);
                    startActivity(marketIntent);

                }
            }
        });

    }

    //Result scan and sn setText result
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == 0) {
            if (resultCode == getActivity().RESULT_OK) {
                etsn.setText(intent.getStringExtra("SCAN_RESULT"));
            } else if (resultCode == getActivity().RESULT_CANCELED) {
                Toast.makeText(getActivity(), "Error scan", Toast.LENGTH_SHORT).show();
            }
        }
    }


    private void updateEmployee(final String url) {
        class UpdateEmployee extends AsyncTask<Void, Void, String> {
            ProgressDialog loading;


            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(getActivity(), "Updating...", "Wait...", false, false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(getActivity(), s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(Void... params) {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put(Const.COLUMN_ID, mId);
                hashMap.put(Const.COLUMN_TP, tvtp.getText().toString());
                hashMap.put(Const.COLUMN_BOX, etbox.getText().toString());
                hashMap.put(Const.COLUMN_MODEL, spModel.getSelectedItem().toString());
                hashMap.put(Const.COLUMN_SN, etsn.getText().toString());
                hashMap.put(Const.COLUMN_COMMENTS, etcomments.getText().toString());

                RequestHandler rh = new RequestHandler();

                String s = rh.sendPostRequest(url, hashMap);

                return s;
            }
        }
        UpdateEmployee ue = new UpdateEmployee();
        ue.execute();
    }


    private void deleteEmployee(final String delUrl, final String delId) {
        class DeleteEmployee extends AsyncTask<Void, Void, String> {
            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(getActivity(), "Updating...", "Wait...", false, false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(getActivity(), s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequestParam(delUrl, delId);
                return s;
            }
        }

        DeleteEmployee de = new DeleteEmployee();
        de.execute();
    }

    private void confirmDeleteEmployee() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setMessage("Are you sure you want to delete this employee?");

        alertDialogBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        deleteEmployee(Url.urlDelItemTpDb, mId);
                    }
                });

        alertDialogBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void readEqDB(final Spinner spModel1, String url, final ProgressDialog PD) {

        PD.show();
        final ArrayList<String> Item_List1 = new ArrayList<>();
        JsonObjectRequest jreq = new JsonObjectRequest(Request.Method.GET, url,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            int success = response.getInt("success");

                            if (success == 1) {
                                JSONArray ja = response.getJSONArray("orders");

                                for (int i = 0; i < ja.length(); i++) {

                                    JSONObject jobj = ja.getJSONObject(i);


                                    Item_List1.add(jobj.getString(Const.COLUMN_MODEL));
                                } // for loop ends


                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, Item_List1);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spModel1.setAdapter(adapter);

                                PD.dismiss();

                            } // if ends

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                PD.dismiss();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(jreq);


    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        if (TextUtils.isEmpty(query)) {
            lv.clearTextFilter();
        } else {
            Item_List1.clear();
            searchDBTP(Url.urlSearchTpDb, query);
//            lv.setFilterText(query.toString());
        }

        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }
}
